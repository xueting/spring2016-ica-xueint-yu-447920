<?php
/**
 * Created by PhpStorm.
 * User: xueting
 * Date: 3/2/16
 * Time: 12:28 PM
 */

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(!isset($_POST['species']) || !isset($_POST['name']) || !isset($_POST['weight'])
        ||!isset($_POST['descriptpion']) || !isset($_POST['picture'])){
        die("ERROR MISSING POST VARIABLES");
    }

    $species = (string) test_input($_POST['species']);
    $name = (string) test_input($_POST['name']);
    $weight = (float) test_input($_POST['weight']);
    $description = (string) test_input($_POST['description']);
    $filename = basename($_FILES['uploadfile']['picture']);

    require('connectDB.php');

    $stmt = $mysqli->prepare("insert into pets (species, name, weight, description, filename) values (?, ?, ?, ?, ?)");

    if (!$stmt) {
        printf("Query failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ssdss', $species, $name, $weight, $description, $filename);
    $stmt->execute();
    $stmt->close();


    echo success;
    header("Location: add-pet.html");